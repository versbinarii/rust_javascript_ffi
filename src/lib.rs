use std::slice;

#[no_mangle]
fn return_incremented(data: *const u8, len: usize) -> *const u8 {
    /* Lets turn it into format thats easier to operate on */
    let array = unsafe { slice::from_raw_parts(data, len as usize) };

    let mut return_array = vec![];

    /* The return values are function of input values */
    for v in array.iter() {
        return_array.push(v + 1);
    }

    /* Obtain a pointer to the array */
    let buf = return_array.as_ptr();

    /* Stop Rust from destroying the variable */
    std::mem::forget(return_array);

    buf
}

#[no_mangle]
fn return_modified(data: *const u8, len: usize, callback: fn(u8) -> u8) -> *const u8 {
    /* Lets turn it into format thats easier to operate on */
    let array = unsafe { slice::from_raw_parts(data, len as usize) };

    let mut return_array = vec![];

    /* The return values are function of input values
    the function is provided by caller */
    for v in array.iter() {
        return_array.push(callback(*v));
    }

    /* Obtain a pointer to the array */
    let buf = return_array.as_ptr();

    /* Stop Rust from destroying the variable */
    std::mem::forget(return_array);

    buf
}

#[repr(C)]
struct ArrayStruct {
    data: *const u8,
    len: usize,
}

#[no_mangle]
fn return_struct(data: *const u8, len: usize, callback: fn(u8) -> bool) -> *const ArrayStruct {
    /* Lets turn it into format thats easier to operate on */
    let array = unsafe { slice::from_raw_parts(data, len as usize) };

    let return_array: Vec<_> = array.iter().filter(|&&v| callback(v)).map(|v| *v).collect();

    /* Obtain a pointer to the array */
    let buf = return_array.as_ptr();
    let len = return_array.len();

    /* Stop Rust from destroying the variable */
    std::mem::forget(return_array);

    unsafe {
        std::mem::transmute(Box::new(ArrayStruct {
            data: buf,
            len: len,
        }))
    }
}
