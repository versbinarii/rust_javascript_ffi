const ffi = require('ffi');
const path = require('path');
const fs = require('fs');

const ref = require('ref');
const ArrayType = require('ref-array');
const StructType = require('ref-struct');

const ByteArray = ArrayType(ref.types.uint8);

const ArrayStruct = StructType({
  data: ByteArray,
  len: ref.types.int
});

const ArrayStructPtr = ref.refType(ArrayStruct);

const CallbackMutate = ffi.Function('int', ['int']);
const CallbackFilter = ffi.Function('bool', ['int']);

const lib = ffi.Library(path.join(__dirname, './target/release/librust_ffi_js'), {
  return_incremented: [ByteArray, [ByteArray, 'int']],
  return_modified: [ByteArray, [ByteArray, 'int', CallbackMutate]],
  return_struct: [ArrayStructPtr, [ByteArray, 'int', CallbackFilter]]
});


const array = [1, 2, 3, 4, 5, 6, 7, 8, 9];


/* Add 1 to every element of js_array and return new array */
(function(js_array){
  const ret_array = lib.return_incremented(js_array, js_array.length);
  
  /* Extract the data from the returned buffer */
  const out = ret_array.buffer.reinterpret(js_array.length).toString('hex');
  console.log('Modify array: ', out);
})(array);



/* Mutate the js_array with the logic in the callback function */
(function(js_array){
  const ret_array = lib.return_modified(js_array, js_array.length, function (num) {
	return num + 3;
  });
    
  const out = ret_array.buffer.reinterpret(js_array.length).toString('hex');
  console.log('With callback: ', out);
})(array);


/* What if the returning array length is changed. It would be great to have the new length returned as well */
(function(js_array) {
  
  const ret_struct = lib.return_struct(js_array, js_array.length, function (num) {
	/* lets filter all even numbers */
	return num % 2;
  });
  
  const struct_value = ret_struct.deref();
  const arr_len = struct_value.len;
  
  const out = struct_value.data.buffer.reinterpret(arr_len).toString('hex');
  console.log('Array bytes: ', out);
  console.log('Array len: ', arr_len);
})(array);


